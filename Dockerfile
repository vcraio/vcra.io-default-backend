FROM nginxinc/nginx-unprivileged:alpine

COPY default.conf /etc/nginx/conf.d
COPY nginx.conf /etc/nginx/
COPY *.html /usr/share/nginx/html/
